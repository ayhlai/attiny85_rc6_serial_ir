# README #

### What is this repository for? ###

* This is a library for serial communications over Infrared (IR) with carrier frequency of 38khz. It has been tested to send data from Attiny 85 (digispark) to Raspberry Pi. The receiver can be any device which have an infrared receiver (with 38khz bandpass filter) configured as serial device.

### How do I get set up? ###

* See examples for how to use the library.  Tested to run at 8Mhz with Baud Rate 2400bps.
* For best accuracy, I suggest to use add CRC checks.  I have tried using CRC-8 Maxim and get very high accuracy with a range of around 6 meters.
* Schematic to be uploaded

### Who do I talk to? ###

* Any thoughts are welcomed to Andrew Lai - ayhlai@gmail.com  